import psycopg2
import psycopg2.extras

from flask import Flask, render_template, request

app = Flask(__name__)

def connectToDB():
    connectionString = 'dbname=dbs_projekt_world_happiness user=postgres password=neuSternchen1?'
    try:
        return psycopg2.connect(connectionString)
        print("ist verbunden")
    except:
        print("ist nicht verbunden")

@app.route('/')
def mainIndex():
    return render_template('index.html', selectedMenu='Home')

@app.route('/world_happiness_report')
def world_happiness_report():

    conn = connectToDB()
    cur = conn.cursor()
    try:
        posts = cur.execute("SELECT * FROM world_happiness_report WHERE year > '2009' AND year < '2017'")
    except:
        print("Error executing select")
    world_happiness_report = cur.fetchall()
    print(posts)
    return render_template('world_happiness_report.html', inhalt=world_happiness_report)

@app.route('/gdp')
def gdp():

    conn = connectToDB()
    cur = conn.cursor()
    try:
        posts = cur.execute("SELECT * FROM gdp WHERE year > '2009' AND year < '2017'")
    except:
        print("Error executing select")
    gdp = cur.fetchall()
    print(posts)
    return render_template('gdp.html', inhalt=gdp)

@app.route('/population_total')
def population_total():

    conn = connectToDB()
    cur = conn.cursor()
    try:
        posts = cur.execute("SELECT * FROM population_total WHERE year > '2009' AND year < '2017'")
    except:
        print("Error executing select")
    population_total = cur.fetchall()
    print(posts)
    return render_template('population_total.html', inhalt=population_total)

@app.route('/population_growth')
def population_growth():

    conn = connectToDB()
    cur = conn.cursor()
    try:
        posts = cur.execute("SELECT * FROM population_growth WHERE year > '2009' AND year < '2017' ")
    except:
        print("Error executing select")
    population_growth = cur.fetchall()
    print(posts)
    return render_template('population_growth.html', inhalt=population_growth)

@app.route('/co2_emission')
def c02_emission():

    conn = connectToDB()
    cur = conn.cursor()
    try:
        posts = cur.execute("SELECT * FROM co2_emission WHERE year > '2009' AND year < '2017'")
    except:
        print("Error executing select")
    co2_emission = cur.fetchall()
    print(posts)
    return render_template('co2_emission.html', inhalt=co2_emission)

if __name__ == '__main__':
    app.run()

